var request = require('request');

module.exports = class Yugioh {
	static match(msg) {

		var tab;

		if (!(tab = msg.content.match(/\<\<([^\>]+)\>\>/g)))
			return;

		for (var i = 0; i < tab.length; i++) {
			var name = tab[i].match(/\<\<(.+)>>/)[1];
			if (name === undefined) {
				continue;
			}
			this.searchSend(name,msg);
		}
	}

	static searchSend(name,msg,type = 0) {
		var cards = [];
		request("https://db.ygoprodeck.com/api/v2/cardinfo.php?name=" + name
				, (err,res,body) => {
					if (err) { throw err; return;}
					if (res && res.statusCode < 400) {
						cards = JSON.parse(body);
						if (!cards.error) {
							msg.channel.send("https://ygoprodeck.com/pics/"
									+ cards[0][0].id +".jpg");
							return;
						}
					}
					msg.channel.send("Aucune carte trouvée. désolée. :cry:");
		});
	}
}
