const Discord = require("discord.js");
const bot = new Discord.Client();
const token = require("./auth.json").discord;
const option = require("./option.json");
const commandes = require("./commandes");
const magic = require("./magic");
const yugioh = require("./yugioh");
const arpenteurs = require("./serveur/arpenteurs");
const ceius = require("./serveur/ceius");

for (i in option.commandes) {
	if (option.commandes[i])
		commandes.add(require("./commandes/"+i));
}

bot.on("ready", function () {
	console.log("Hello !!! je suis ready");
	bot.user.setPresence({ game: { name: 'préparer le café' }});
	arpenteurs(bot);
        ceius(bot);
});

bot.on("message", function (msg) {
	if (msg.author.username === bot.user.username) {
		return;
	}
	var user = msg.mentions.users.first();
	if (user && user.username == bot.user.username) {
		msg.reply("Un petit café ?");
	}

	commandes.match(msg);

	magic.match(msg);
	yugioh.match(msg);

});

bot.on("guildCreate", function(guild) {
	guild.defaultChannel.send("CouCou @everyone, un petit café ?");
})

bot.login(token);
