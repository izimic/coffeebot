const {Client, RichEmbed} = require('discord.js');

module.exports = class Card {

	constructor(msg,card,type) {
		this.msg = msg;
		this.card = card;
		this.type = type;
		this.msg2;
		this.authorId;
	}

	getName() {
		return "**" + this.card.set.toUpperCase() 
			+ "** " + (this.card.printed_name || this.card.name);
	}

	getTitle() {
		var str = this.getName();
		if (this.card.mana_cost != "") {
			str += " " + this.card.mana_cost + " (" + this.card.cmc + ")";
		}
		return str;
	}

	getPower() {
		if (this.card.power) {
			return this.card.power + "/" + this.card.toughness;
		}
		else if (this.card.loyalty) {
			return this.card.loyalty;
		}
		else {
			return "";
		}
	}

	imagePage(embed) {
		embed.setTitle(this.getName())
			.setURL(this.card.scryfall_uri)
			.setImage(this.card.image_uris.large);
	}

	textPage(embed) {
		embed.setTitle(this.getTitle())
			.setThumbnail(this.card.image_uris.art_crop)
			.setFooter(this.card.flavor_text || "")
			.setURL(this.card.scryfall_uri)
			.setDescription("**" + this.card.type_line + "**" + "\n"
					+ (this.card.printed_text || this.card.oracle_text) + "\n"
					+ this.getPower());
	}

	legalPage(embed) {
		embed.setTitle(this.getName())
			.setURL(this.card.scryfall_uri);
		for (var i in this.card.legalities) {
			embed.addField(i,this.card.legalities[i],true);
		}
	}

	getEmbed() {
		const embed = new RichEmbed();
		if (this.type == 0) {
			this.imagePage(embed);
		}
		else if (this.type == 1) {
			this.textPage(embed);
		}
		else if (this.type == 2) {
			this.legalPage(embed);
		}
		return embed;
	}

	createReact(emoji,action,callback) {
		this.msg2.react(emoji).then(callback).catch(console.error);

		const Filter = (reaction,user) => reaction.emoji.name === emoji && user.id === this.authorId;
		const react = this.msg2.createReactionCollector(Filter,{time: 60000});
		react.on("collect", action);
	}


	send() {
		this.msg.channel.send(this.getEmbed(0))
			.then( msg2 => {
					this.authorId = this.msg.author.id;
					this.msg2 = msg2;
					var cardControl = this;

					var actionImage = (f) => {
						if (cardControl.type != 0) {
							cardControl.type = 0;
							cardControl.msg2.edit(cardControl.getEmbed());
						}
					}

					var actionText = (f) => {
						if (cardControl.type != 1) {
							cardControl.type = 1;
							cardControl.msg2.edit(cardControl.getEmbed());
						}
					}

					var actionLegal = (f) => {
						if (cardControl.type != 2) {
							cardControl.type = 2;
							cardControl.msg2.edit(cardControl.getEmbed());
						}
					}

					var actionCancel = (f) => {
						cardControl.msg2.delete(0);
					}

					this.createReact("🎴",actionImage,() => {
						this.createReact("📜",actionText,() => {
							this.createReact("👮",actionLegal,() => {
								this.createReact("❌",actionCancel);
							})
						})
					});
			})
			.catch(console.error);
	}
}
