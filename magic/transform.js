const Card = require("./card.js");
const {Client, RichEmbed} = require('discord.js');

module.exports = class Transform extends Card {
	constructor(msg,card,type) {
		super(msg,card.card,type);
		this.fullCard = card;
		this.face = 1;
		this.setFace();
	}

	setFace() {
		this.face = (this.face + 1) % 2;
		this.card = this.fullCard.card_faces[this.face];
		this.card.set = this.fullCard.set;
		this.card.legalities = this.fullCard.legalities; 
	}

	send() {
		this.msg.channel.send(this.getEmbed(0))
			.then( msg2 => {
					this.authorId = this.msg.author.id;
					this.msg2 = msg2;
					var cardControl = this;

					var actionImage = (f) => {
						if (cardControl.type != 0) {
							cardControl.type = 0;
							cardControl.msg2.edit(cardControl.getEmbed());
						}
					}

					var actionChange = (f) => {
						cardControl.setFace();
						cardControl.msg2.edit(cardControl.getEmbed());
					}

					var actionText = (f) => {
						if (cardControl.type != 1) {
							cardControl.type = 1;
							cardControl.msg2.edit(cardControl.getEmbed());
						}
					}

					var actionLegal = (f) => {
						if (cardControl.type != 2) {
							cardControl.type = 2;
							cardControl.msg2.edit(cardControl.getEmbed());
						}
					}

					var actionCancel = (f) => {
						cardControl.msg2.delete(0);
					}

					this.createReact("🎴",actionImage,() => {
						this.createReact("📜",actionText,() => {
							this.createReact("🔄",actionChange,() => {
								this.createReact("👮",actionLegal,() => {
									this.createReact("❌",actionCancel);
								})
							})
						})
					});
			})
			.catch(console.error);
	}
}
