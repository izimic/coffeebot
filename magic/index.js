const mtg = require('mtgsdk');
const Scry = require("scryfall-sdk");
const {Client, RichEmbed} = require('discord.js');
const Card = require("./card")
const CardTransform = require("./transform")

module.exports = class Magic {
	static match(msg) {

		var tab;

		if (!(tab = msg.content.match(/\[\[([^\]]+)\]\]/g)))
			return;

		for (var i = 0; i < tab.length; i++) {
			var name = tab[i].match(/\[\[(.+)]]/)[1];
			if (name === undefined) {
				continue;
			}
			this.searchSend(name,msg);
		}
	}

	static message(cards,msg) {
		if (cards === undefined || msg === undefined)
			return;

		if (cards.length == 0) {
			return msg.channel.send("Aucune carte trouvée. désolée. :cry:");
		}
		else if (cards.length == 1) {
			var card = cards[0];
			var cardControl;
			if (card.layout == "transform") {
				cardControl = new CardTransform(msg,card,0);
			}
			else {
				cardControl = new Card(msg,card,0);
			}
			cardControl.send();
		}
		else if (cards.length > 100) {
			msg.channel.send("Plus de cent cartes trouvées. Affinez votre recherche. :wink: ")
		}
		else if (cards.length > 1) {
			var str = "";
			cards.forEach( card => {
					str += card.set + " " + card.name +  "\n";
			});
			msg.channel.send(str).catch(console.error);
		}
	}

	static searchSend(name,msg) {
		var cards = [];
		Scry.Cards.search(name+" prefer:newest (lang:fr or lang:en) include:extras ","")
		.on("data",result => cards.push(result))
		.on("end", () => { 
			this.message(cards,msg);
		});
	}
}
