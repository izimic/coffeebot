commandes = [];

module.exports = class Commandes {
	static add(commande) {
		commandes.push(commande);
	}

	static match (msg) {
		var args = msg.content.split(/ +/g);
		var s = args[0].substring(0,1);
		args[0] = args[0].substring(1);
		if (s !== "!") {
			return false;
		}
		var r = false;
		for (var i = 0; !r && i < commandes.length; i++) {
			r = commandes[i].match(args[0]);
			if (r) {
				commandes[i].action(msg,args);
			}
		}
	}
}
