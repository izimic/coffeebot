module.exports = [
	"",
	
	"   ,==========Y===\n"+
	"   ||  /\n"+
	"   || /\n"+
	"   ||/\n"+
	"   ||\n"+
	"   ||\n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	,
	
	"   ,==========Y===\n"+
	"   ||  /      |\n"+
	"   || /       |\n"+
	"   ||/        \n"+
	"   ||\n"+
	"   ||\n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	,
	
	"   ,==========Y===\n"+
	"   ||  /      |\n"+
	"   || /       |\n"+
	"   ||/        O\n"+
	"   ||\n"+
	"   ||\n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	,
	
	"   ,==========Y===\n"+
	"   ||  /      |\n"+
	"   || /       |\n"+
	"   ||/        O\n"+
	"   ||         |\n"+
	"   ||\n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	,
	
	"   ,==========Y===\n"+
	"   ||  /      |\n"+
	"   || /       |\n"+
	"   ||/        O\n"+
	"   ||        /|\\\n"+
	"   ||        \n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	,
	
	"   ,==========Y===\n"+
	"   ||  /      |\n"+
	"   || /       |\n"+
	"   ||/        O\n"+
	"   ||        /|\\\n"+
	"   ||        /|\n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	,
	
	"   ,==========Y===\n"+
	"   ||  /      |\n"+
	"   || /       |\n"+
	"   ||/        O\n"+
	"   ||        /|\\\n"+
	"   ||        /|\n"+
	"   ||\n"+
	"  /||\n"+
	" //||\n"+
	"============"
	
]
