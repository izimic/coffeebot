const {Client, RichEmbed} = require("discord.js");
const fs = require("fs");
const imgs = require("./img.js");
const mots = require("./mots.json")

var erreur = 0;
var mot = "";
var lettres = {};
var partie = false;

function getstr() {
	var str = "```" + imgs[erreur] + "\n";
	if (erreur > 5 ) {
		str += mot + "\nPerdu";
		partie = false;
	}
	else if (mot) {
		var g = true;
		for (var i = 0; i < mot.length; i++) {
			if (lettres[mot[i]])
				str += mot[i];
			else {
				str += "_";
				g = false;
			}
		}
		if (g) {
			str += "\nGagné"
			partie = false
		}
	}
	str += "```";

	str = ((!partie) ?
		"`!pendu start` pour commencer une partie.\n":
		"`!pendu put <lettre>` pour tester une lettre.\n")+
		str;

	return str;
}

module.exports = class Pendu {
	static match(str) {
		return str === "pendu";
	}

	static action(msg,args) {

		if (args[1] == "start") {
			if (!partie) {
				erreur = 0;
				lettres = {};
				var num = Math.floor(Math.random() * Math.floor(mots.length+1));
				mot = mots[num];
				partie = true;
			}
		}
		else if (args[1] == "put") {
			if (partie) {
				var trouve = false;
				var l = args[2].toUpperCase();
				for (var i = 0; i < mot.length; i++) {
					if (mot[i] == l) trouve = true;
				}
				if (trouve)
					lettres[l] = true;
				else
					erreur++;
			}
		}
		msg.channel.send(getstr());
		return;
	}
}
