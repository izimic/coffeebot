module.exports = class Ping {
	static match(str) {
		return str === "ping";
	}

	static action(msg,args) {
		return msg.reply("pong");
	}
}
