module.exports = class Roulette {
	static match (str) {
		return str === "roulette"
	}
	static action (msg, args) {
		var members = msg.channel.members.array();
		var users = [];
		for (var i = 0; i < members.length; i++) {
			if (!members[i].user.bot && members[i].user.presence.status === "online") {
				users.push(members[i].nickname || members[i].user.username);
			}
		}
		var r = Math.floor(Math.random() * users.length);

		return msg.channel.send("Le destin choisit : " + users[r] + " !!!");
			
	}
}
