module.exports = class Spam {
	static match(str) {
		return str === "spam";
	}

	static action(msg,args) {
		if (msg.channel.type !== "text") return;
		console.log(args.shift());
		console.log(args.shift());
		var user = msg.mentions.users.first();
		if (!user || user.username.toLowerCase() == "izimic") {
			user = msg.author;		
		}
	
		var t = args.shift()
		console.log(t);
		var nb = parseInt(t);
		if (!nb || Number.isNaN(nb))
			nb = 10;

		var text = args.join(" ");
		for (var i = 0; i < nb; i++)
			user.send(text);
	}

}
