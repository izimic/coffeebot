module.exports = class Pileface {
	static match(str) {
		return str === "pileface";
	}

	static action(msg,args) {
		if (Math.floor(Math.random() * 2) == 0) {
			msg.channel.send({
				files: [{
					attachment: "./images/pile.jpg",
					name: "pile.jpg"
				}]
			})
				.catch(console.error)
		}
		else {
			msg.channel.send({
				files: [{
					attachment: "./images/face.jpg",
					name: "face.jpg"
				}]
			})
				.catch(console.error)
		}
	}
}
