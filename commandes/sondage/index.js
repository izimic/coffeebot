const getName = require("../../getName.js")
const data = require("./data");

var nom = "sondage";
var commandes = "aide|liste|creer|vote|voir|fermer|supprimer";

var aides = {
	"aide" : "**usage**: !"+nom+" aide ["+commandes+"]\n"+
		"\t**aide**: affiche ce message\n"+
		"\t**liste**: affiche la liste des sondages\n"+
		"\t**creer**: crée un nouveau sondage\n"+
		"\t**vote**: vote à un sondage\n"+
		"\t**voir**: voir un sondage"+
		"\t**fermer**: ferme un sondage\n"+
		"\t**supprimer**: supprime un sondage",
	"liste" : "**usage**: !"+nom+" liste\n"+
		"\taffiche la liste des sondages",
	"creer" : "**usage**: !"+nom+" creer <sondage> [options...]\n"+
		"\tcrée un nouveau sondage\n"+
		"\t**sondage**: sondage à créer\n"+
		"\t**options**: les differentes options du sondage\n"+
		"\t\t par defaut oui et non",
	"vote" : "**usage**: !"+nom+" vote <sondage> <option>\n"+
		"\t vote à un sondage\n"+
		"\t **sondage**: sondage à voter\n"+
		"\t **option**: option du vote",
	"voir" : "**usage**: !"+nom+" voir <sondage>\n" +
		"\t voir un sondage\n" +
		"\t **sondage**: sondage à voir",
	"fermer" : "**usage**: !"+nom+" fermer <sondage>\n"+
		"\tferme un sondage\n"+
		"\t**sondage**: sondage à fermer",
	"supprimer" : "**usage**: !"+nom+" supprimer <sondage>\n"+
		"\tsupprime un sondage\n"+
		"\t**sondage**:sondage à supprimer"
}

var usage = "**usage**: !"+nom+" ["+commandes+"]";

var functions = {
	"aide" : (msg,args) => {
		msg.channel.send(args.length == 3 && aides[args[2]] ?
			aides[args[2]]:
			aides["aide"]);
	},
	"liste" : (msg,args) => {
		data.all(msg);
	},
	"creer" : (msg,args) => {
		if (args.length < 3){
			msg.channel.send(aides["creer"]);
		}
		var nom = args[2];
		var option;

		if (args.length == 3) {
			option = ["oui","non"];
		}
		else {	
			option = args.slice(3);
		}
		data.creer(msg,nom,getName(msg),option);
	},
	"vote" : (msg,args) => {
		args.length == 4  ?
			data.vote(msg,args[2],getName(msg),args[3]) :
			msg.channel.send(aides["vote"]);
	},
	"voir" : (msg,args) => {
		args.length == 3 ?
			data.voir(msg,args[2]) :
			msg.channel.send(aides["voir"]);
	},
	"fermer": (msg,args) => {
		args.length == 3 ?
			data.fermer(msg,args[2]) :
			msg.channel.send(aides["fermer"]);
	},
	"supprimer": (msg,args) => {
		args.length == 3 ?
			data.supprimer(msg,args[2]) :
			msg.channel.send(aides["supprimer"]);
	}
}

module.exports = class Sondage {

	static match(str) {
		return str === "sondage";
	}

	static action(msg,args) {
		args[1] && functions[args[1]] ?
		functions[args[1]](msg,args) :
		msg.channel.send(usage);
	}
}
