const fs = require("fs");
const getName = require("../../getName.js")
const {Client, RichEmbed} = require('discord.js');

var data = {};
var file = "commandes/sondage/data.json";
fs.readFile(file,"utf-8",(err,str) => {
	if (err) {throw err;}
	data = JSON.parse(str || "{}");
});

function save() {
	fs.writeFile(file,JSON.stringify(data),"utf-8",(err) => {
		if (err) console.log(err);
	});
}

function existe(msg,nom,callback) {
	data[nom] ? callback() : msg.channel.send("le sondage "+nom+" n'existe pas.");
}

function nexiste(msg,nom,callback) {
	data[nom] ? msg.channel.send("le sondage existe déjà."): callback();
}

function options(nom) {
	var opts = {};
	data[nom].options.forEach((e) => {
		opts[e] = {"nombre" : 0, "noms" : ""};
	})
	data[nom].votes.forEach((e) => {
		opts[e.option].nombre++;
		opts[e.option].noms += e.nom + "; ";
	});
	return opts;
}

function embed(nom,opts) {
	if(!opts)
		opts = options(nom)

	var nb = 0;
	var embed = new RichEmbed();
	embed.setTitle("__**" + nom + "**__")
		.setFooter("by " + data[nom].auteur);
		for (var i in opts) {
			nb++;
			embed.addField(nb + " **:** " +i, "—" + opts[i].nombre + " [ " + opts[i].noms + "]");
		}
	return embed;
}

var emojis = ["1⃣","2⃣","3⃣","4⃣","5⃣","6⃣","7⃣","8⃣","9⃣","🔟"];
var yesnoEmojis = ["🇾","🇳"];

function send (msg,nom) {
	var opts = options(nom);

	var keys = Object.keys(opts);
	var max = keys.length < 11 ? keys.length : 10;
	var i = 0;

	msg.channel.send(embed(nom,opts)).then(msg2 => {
		var recFunc = function () {
			if (i >= max) return;

			var emoji;
			var opt = keys[i];
			if (opt == "oui" || opt == "non")
				emoji = yesnoEmojis[i];
			else
				emoji = emojis[i];



			const filter = (reaction,user) => {
				if (reaction.emoji.name === emoji
					&& user.id != msg2.author.id) {
					Data.vote(msg,nom,user.username,opt);
					msg2.edit(embed(nom));
					return true;
				}
				else {
					return false;
				}
			}
			const react = msg2.createReactionCollector(filter,{time: 7*24*60*60*1000});

			react.on("collect", (e,c) => {
				/*console.log(e);
				console.log(c);
				Data.vote(msg,nom,getName(msg),opt);
				msg2.edit(embed(nom));*/
			});

			i++;

			msg2.react(emoji).then(recFunc).catch(console.error);

		};

		recFunc();
	});
}

class Data {
	static all(msg) {
		var re = "__liste__:\n"
		for (var i in data) {
			re += "**" + i + "** *"+ (data[i].ouvert ? "ouvert" : "fermer") + "*\n";
		}
		msg.channel.send(re);
	}

	static creer(msg,nom,auteur,options) {
		return nexiste(msg,nom, () => {
			data[nom] = {
				"auteur": auteur,
				"options": options,
				"ouvert": true,
				"votes" : []
			}
			save();
			msg.channel.send("le sondage "+nom+" a été créé. :ok_hand:");
			send(msg,nom);
		});
	}

	static vote(msg,nom,auteur,option) {
		return existe(msg,nom, () => {
			var i = data[nom].options.indexOf(option);
			if (i < 0) {
				var r = "l'option " + option + " n'existe pas\n"
					+ "selectionez une option parmi celle-ci:\n";
				data[nom].options.forEach((e) => {
					r += e + "\n";
				})
				msg.channel.send(r);
				return;
			}
			var i = data[nom].votes.findIndex( (e) => {
				return e.nom == auteur;
			})
			if ( i < 0) {
				data[nom].votes.push({ "nom": auteur, "option": option});
			}
			else {
				data[nom].votes[i].option = option;
			}
			save();
			msg.channel.send(auteur + " a voté " + option + " au sondage " + nom + "."); 
		});
	}

	static voir(msg,nom) {
		return existe(msg,nom, () => {
			send(msg,nom);

			//msg.channel.send(embed(nom,data[nom].auteur,opt));
		})
	}

	static fermer(msg,nom) {
		return existe(msg,nom, () => {
			data[nom].ouvert = false;
			save();
			msg.channel.send("le sondage "+nom+" a été fermé. :ok_hand:");			
		});
	}

	static supprimer(msg,nom) {
		return existe(msg,nom, () => {
			delete data[nom];
			save();
			msg.channel.send("le sondage "+nom+" a été supprimé. :ok_hand:");			
		});
	}
}

module.exports = Data;
