function getRandomInt(min,max) {
	return min + Math.floor(Math.random() * Math.floor(max-min+1));
}

module.exports = class Dice {
	static match(str) {
		return str === "roll"; 
	}

	static action(msg,args) {
		var name = msg.member ? msg.member.nickname || msg.author.username : msg.author.username;
		var msgre = "";
		var total = 0;
		var data;
		for (var index = 1; index < args.length;index++) {
			if (args[index] && (data = args[index].match(/([0-9]+)d([0-9]+)(\+([0-9]+))?/i))) {
				if (data[1] == 0 || data[2] == 0 ) {
					msg.channel.send("RNG (random number god) m'a interdit de faire ça. pardon désolé :cry:").catch(console.error);
				}

				else if (data[2] == 1) {
					msgre = "Le suspens est intenable \n" + name + " lance et … \n";
					var nb = parseInt(data[1]);
					if (data[3] != null) {
						 nb += parseInt(data[4]);
					}
					msgre += nb + " !!! C'est incroyable !!" ;
					msg.channel.send(msgre).catch(console.error);
				}

				else {

					msgre = name + " lance ";
					if (data[1] > 1) {
						msgre += data[1] + " dés ";
					}
					else {
						msgre += "un dé ";
					}
					if (data[2] > 1 ) {
						msgre += "à " + data[2] + " faces\n"
					}

					total = 0;
					for (var i = 0; i < data[1];i++) {
						var nb = getRandomInt(1,data[2]);
						total += nb;
						if (data[3] != null || data[1] > 1) {
							msgre += nb + " "
						}
					}

					if (data[3] != null) {
						msgre += "+ " + data[4] + " ";
						total += parseInt(data[4]);
					}
					if (data[1] > 1) {
						msgre += "\n"
					}
					msgre += "=> " + total;

					msg.channel.send(msgre).catch(console.error);
				}

			}
			else {
				msg.reply(args[index] + " => ? ")
					.catch(console.error);
			}
		}
		return;
	}
}
