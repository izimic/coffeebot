const {google} = require("googleapis");
const youtube = google.youtube('v3');
const auth = require("../auth.json").youtube;


module.exports = class Desespoir {
	static match(str) {
		return str === "desespoir";
	}

	static action(msg,args) {
		var params = {
			"auth" : auth,
			"maxResult" : 50,
			"playlistId" : "PLYEOaZM4dJgMpZu1AXwgElNtcFHDXBbaZ",
			"part" : "snippet"
		};

		youtube.playlistItems.list(params, (err, res) => {
			if (err) return console.log(err);

			var i = Math.floor(Math.random() * res.data.pageInfo.totalResults);
			var send = function(err,res) {
				if (err) return console.log(err);
				if (i <= 5) {
					msg.channel.send("https://www.youtube.com/watch?v="+res.data.items[i].snippet.resourceId.videoId+"&list="+res.data.items[i].snippet.playlistId);
				}
				else {
					i -= 5;
					params["pageToken"] = res.data.nextPageToken;
					youtube.playlistItems.list(params, send);
				}
			}
			send(err,res);
		});
	}
}
