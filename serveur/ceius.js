

module.exports = function(client) {

	var guildAS = client.guilds.find( g => { return g.id == "636649349425790997"});
	if (!guildAS) return console.log("fail guild");
	var channelBienvenue = guildAS.channels.find(c => { return c.id == "636654504414019624"});
	if (!channelBienvenue) return console.log("fail channel.")
	channelBienvenue.fetchMessage("637286666637279232").then( message => {

		const filter = (reaction,user) => reaction.emoji.name === "✅";
		const react = message.createReactionCollector(filter);
		react.on("collect", (mr,c) => {
			mr.users.forEach(user => {
				guildAS.fetchMember(user).then(gm => {
					gm.addRole("637287452402253835").catch(console.error);
				}).catch(console.error);
			});
		});
	}).catch(console.error);

	client.on("guildMemberAdd", function(guildMember) {
		console.log(guildMember.guild.id);
		if (guildMember.guild.id != "636649349425790997") {
			return;
		}
		guildMember.send("Bienvenue sur le serveur discord du Cercle des édutiants en Informatique de l'Univercité de Strasbourg !!!" +
                                "\n N'hésite pas à nous faire une petite présentation dans #présentation."+
				"\n Et avant de pouvoir accéder au serveur tu dois accepter les règles d'utilisation." +
				"\n Dans #bienvenue clic sur ✅ et tu pourras accéder au serveur.")
	})

	client.on("guildMemberRemove", function(guildMember) {
		console.log(guildMember.guild.id);
		if (guildMember.guild.id != "636649349425790997") {
			return;
		}
		var channel = guildAS.channels.find(c => { return c.id == "636649349425790999"});

		channel.send(guilMember.nickname || guilMember.user.username + " est partit.")

	})

}
