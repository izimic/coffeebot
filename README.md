# CoffeeBot

Le bot qui fait presque le café.  
CoffeeBot est un bot [discord](http://discordapp.com).

## Installation

Télécharger les sources:
```shell
git clone https://gitlab.com/izimic/coffeebot.git
cd coffeebot
```
Créez un fichier auth.json avec ce code:
```json
{
    "discord" : "<Le token de votre bot>",
    "youtube" : "<La cle d'api pour youtube>"
}
```
Et pour finir l'installation:
```shell
npm install
node index.js
```

## Liste des Commandes

#### Ping

    !ping
 
Permet de ping le bot.

#### Pile ou Face

    !pileface

Joue à pile ou face.

#### Roll

    !roll XdY

Lance X dés à Y face

    !roll XdY+Z

Lance X dés à Y face et ajoute Z au résultat.

#### Roulette

    !roulette

Choisis, au hasard, un membre du salon.

### Sondage

	!sondage <commande>

Permet de crée des sondages et de les gerer.

### Magic
```
[[ NomCarte ]]
```
Renvois l'image de NomCarte ou une liste de nom si le nom est partiel. Utilise la [syntax sryfall](https://scryfall.com/docs/syntax).  
Avec les emojis on peut changer de page entre:
- image: l'image de la carte.
- text: la carte en format text.
- legalités: la legalités de la carte selon les formats.

### Yu-gi-oh
```
[[ NomCarte ]]
```
Renvois l'image de NomCarte.  
NomCarte doit être le nom exact.

## Copyright
This bot is [free software](http://www.gnu.org/philosophy/free-sw.html) and is distributed under the [GPLv3 licence](http://www.gnu.org/copyleft/gpl-3.0.html).
