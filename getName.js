module.exports = function(msg) {
	return msg.member ? msg.member.nickname || msg.author.username : msg.author.username;
}
